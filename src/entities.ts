export interface Publish{
    id?: number;
    users: string;
    article: string;
    likes?: number;
    sectionId?:number;
    section: Section;
}

export interface Section{
    id?: number;
    titre: string;
}

export interface User{
    id?: number;
    email: string;
    password: string;
    role?: string;
}