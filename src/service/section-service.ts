import axios from "axios";
import type { Section } from "@/entities";
export async function fetchAllSection() {
    const response = await axios.get<Section[]>('http://localhost:8080/api/section');
    return response.data;
}
