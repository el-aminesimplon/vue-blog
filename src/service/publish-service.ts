import axios from "axios";
import type { Publish } from "@/entities";
export async function fetchAllPublish() {
    const response = await axios.get<Publish[]>('http://localhost:8080/api/publish');
    return response.data;
}

export async function fetchOnePublish(id:any) {
    const response = await axios.get<Publish>('http://localhost:8080/api/publish/'+id);
    return response.data;
}

export async function postPublish(publish: Publish) {
    const response = await axios.post<Publish>('http://localhost:8080/api/publish', publish);
    return response.data;
}


export async function deletePublish(id:any) {
    await axios.delete<void>('http://localhost:8080/api/publish/'+id);
}

export async function fetchPublishSection(id:any) {
    const response = await axios.get<Publish[]>('http://localhost:8080/api/publish/section/'+id);
    return response.data;
}

export async function addLike(id: any) {
    const response = await axios.put<Publish>("http://localhost:8080/api/publish/" + id + "/like");
    return response.data;
}
