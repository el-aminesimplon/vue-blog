import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import PublishAllView from '@/views/PublishAllView.vue'
import SinglePublishView from '@/views/SinglePublishView.vue'
import SectionView from '@/views/SectionView.vue'
import LoginView from '@/views/LoginView.vue'
import InscriptionView from '@/views/InscriptionView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: HomeView
    },
    {
      path: '/all',
      component: PublishAllView
    },
    {
      path: '/singlePublish/:id',
      component: SinglePublishView
    },
    {
      path: '/section/:id',
      component: SectionView
    },
    {
      path: '/login',
      component: LoginView
    },
    {
      path: '/inscription',
      component: InscriptionView
    }
  ]
})

export default router
